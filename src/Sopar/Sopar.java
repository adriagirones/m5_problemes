package Sopar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Sopar {
	
	public static ArrayList<Familiars> sortida = new ArrayList<>();
	public static ArrayList<Familiars> meta = new ArrayList<>();
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int ncasos = sc.nextInt();
		for (int i = 0; i < ncasos; i++) {
			int familiarsBambino = sc.nextInt();
			int familiarsEx = sc.nextInt();
			System.out.println(resposta(familiarsBambino, familiarsEx));
		}
	}

	public static String resposta(int familiarsBambino, int familiarsEx) {
		sortida.clear();
		meta.clear();
		int bambinoFamMax = familiarsBambino;
		int exFamMax = familiarsEx;
		Familiars left = new Familiars(familiarsBambino, familiarsEx);
		Familiars right = new Familiars(0,0);
		int cont = go(left, right, 0, true, bambinoFamMax, exFamMax);
		if(cont == -1) {
			return "NOS HEMOS QUEDADO SIN CENA";
		}
		else {
			return String.valueOf(cont);
		}
	}

	private static int go(Familiars left, Familiars right, int cont, boolean torn, int bambinoFamMax, int exFamMax) {
		if(comp(left, sortida) && torn) {
			return -1;
		}
		else if(comp(right, meta) && !torn) {
			return -1;
		}
		else if(left.famBamb == 0 && left.famEx == 0 && right.famBamb == bambinoFamMax && right.famEx == exFamMax) {
			return cont;
		}
		else if(left.famBamb > bambinoFamMax || left.famEx > exFamMax || right.famBamb > bambinoFamMax || right.famEx > exFamMax) {
			return -1;
		}
		else if(left.famBamb < 0 || left.famEx < 0 || right.famBamb < 0 || right.famEx < 0) {
			return -1;
		}
		else if((left.famBamb>0 && left.famBamb<left.famEx) || (right.famBamb<right.famEx && right.famBamb>0)) {
			return -1;
		}
		else {
			cont++;
			ArrayList<Integer> min = new ArrayList<>();
			if(torn) {
				sortida.add(left);
				Familiars l1 = new Familiars(left.famBamb, left.famEx-2);
				Familiars r1 = new Familiars(right.famBamb, right.famEx+2);
				min.add(go(l1, r1, cont, !torn, bambinoFamMax, exFamMax));
				Familiars l2 = new Familiars(left.famBamb-2, left.famEx);
				Familiars r2 = new Familiars(right.famBamb+2, right.famEx);
				min.add(go(l2, r2, cont, !torn, bambinoFamMax, exFamMax));
				Familiars l3 = new Familiars(left.famBamb, left.famEx-1);
				Familiars r3 = new Familiars(right.famBamb, right.famEx+1);
				min.add(go(l3, r3, cont, !torn, bambinoFamMax, exFamMax));
				Familiars l4 = new Familiars(left.famBamb-1, left.famEx);
				Familiars r4 = new Familiars(right.famBamb+1, right.famEx);
				min.add(go(l4, r4, cont, !torn, bambinoFamMax, exFamMax));
				Familiars l5 = new Familiars(left.famBamb-1, left.famEx-1);
				Familiars r5 = new Familiars(right.famBamb+1, right.famEx+1);
				min.add(go(l5, r5, cont, !torn, bambinoFamMax, exFamMax));
			}
			else {
				meta.add(right);
				Familiars l1 = new Familiars(left.famBamb, left.famEx+2);
				Familiars r1 = new Familiars(right.famBamb, right.famEx-2);
				min.add(go(l1, r1, cont, !torn, bambinoFamMax, exFamMax));
				Familiars l2 = new Familiars(left.famBamb+2, left.famEx);
				Familiars r2 = new Familiars(right.famBamb-2, right.famEx);
				min.add(go(l2, r2, cont, !torn, bambinoFamMax, exFamMax));
				Familiars l3 = new Familiars(left.famBamb, left.famEx+1);
				Familiars r3 = new Familiars(right.famBamb, right.famEx-1);
				min.add(go(l3, r3, cont, !torn, bambinoFamMax, exFamMax));
				Familiars l4 = new Familiars(left.famBamb+1, left.famEx);
				Familiars r4 = new Familiars(right.famBamb-1, right.famEx);
				min.add(go(l4, r4, cont, !torn, bambinoFamMax, exFamMax));
				Familiars l5 = new Familiars(left.famBamb+1, left.famEx+1);
				Familiars r5 = new Familiars(right.famBamb-1, right.famEx-1);
				min.add(go(l5, r5, cont, !torn, bambinoFamMax, exFamMax));		
			}
			ArrayList<Integer> ret = new ArrayList<>();
			for (int i = 0; i < min.size(); i++) {
				if(min.get(i) != -1) {
					ret.add(min.get(i));
				}		
			}
			return ret.size() > 0 ? Collections.min(ret) : -1;
		}
	}
	
	private static boolean comp(Familiars l, ArrayList<Familiars> s) {
		boolean flag = false;
		for (int i = 0; i < s.size(); i++) {
			if(s.get(i).famEx == l.famEx && s.get(i).famBamb == l.famBamb) {
				flag = true;
			}
		}
		return flag;
	}

}