package Sopar;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestSopar {

	@Test
	void test() {
		assertEquals("11", Sopar.resposta(3, 3));
		assertEquals("NOS HEMOS QUEDADO SIN CENA", Sopar.resposta(2, 3));
		assertEquals("17", Sopar.resposta(5, 3));
		assertEquals("NOS HEMOS QUEDADO SIN CENA", Sopar.resposta(10, 10));
	}
	
	@Test
	void test2() {
		assertEquals("11", Sopar.resposta(3, 3));
		assertEquals("NOS HEMOS QUEDADO SIN CENA", Sopar.resposta(2, 3));
		assertEquals("17", Sopar.resposta(5, 3));
		assertEquals("NOS HEMOS QUEDADO SIN CENA", Sopar.resposta(10, 10));
	}

}
