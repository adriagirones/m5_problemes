package Rellotge;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestRellotge {

	@Test
	void testsBasics() {
		assertEquals(1, Rellotge.resposta(2, 15, 3600));
		assertEquals(1, Rellotge.resposta(2, 15, 7200));
		assertEquals(2, Rellotge.resposta(2, 15, 7500));
		assertEquals(3, Rellotge.resposta(11, 30, 8000));
	}
	
	@Test
	void testsNumGrans() {
		assertEquals(4999, Rellotge.resposta(0, 0, 18000000));
		assertEquals(5000, Rellotge.resposta(11, 59, 18000000));
		assertEquals(194037, Rellotge.resposta(11, 59, 698535784));
	}
	
	@Test
	void testsCasosRars() {
		assertEquals(2-4, Rellotge.resposta(0, 0, 86400));
		assertEquals(23, Rellotge.resposta(0, 0, 86399));
		assertEquals(24, Rellotge.resposta(0, 0, 86401));
		assertEquals(0, Rellotge.resposta(0, 30, -5006482));
	}

}
