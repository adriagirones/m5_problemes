package Rellotge;

import java.util.Scanner;

public class Rellotge {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int ncasos = sc.nextInt();
		for (int i = 0; i < ncasos; i++) {
			int h = sc.nextInt();
			int m = sc.nextInt();
			int sT = sc.nextInt();
			System.out.println(resposta(h, m, sT));
		}
	}

	public static int resposta(int h, int m, int sT) {
		int s = 0;
		int cont = 0;
		for (int j = 0; j < sT; j++) {
			s++;
			if(s==60) {
				s=0;
				m++;
			}
			if(m==60) {
				m=0;
				h++;
			}
			if(h==12) {
				h=0;
			}
			cont += comp(m, h, s);
		}
		//System.out.println(h+":"+m+":"+s);
		return cont;
		
	}

	private static int comp(int m, int h, int s) {
		if(m==0 && h==0 && s==0) {
			return 1;
		}
		else if(m==5 && h==1 && s==0) {
			return 1;
		}
		else if(m==10 && h==2 && s==0) {
			return 1;
		}
		else if(m==15 && h==3 && s==0) {
			return 1;	
		}
		else if(m==20 && h==4 && s==0) {
			return 1;
		}
		else if(m==25 && h==5 && s==0) {
			return 1;
		}
		else if(m==30 && h==6 && s==0) {
			return 1;
		}
		else if(m==35 && h==7 && s==0) {
			return 1;
		}
		else if(m==40 && h==8 && s==0) {
			return 1;
		}
		else if(m==45 && h==9 && s==0) {
			return 1;
		}
		else if(m==50 && h==10 && s==0) {
			return 1;
		}
		else if(m==55 && h==11 && s==0) {
			return 1;
		}
		else {
			return 0;
		}
	}

}
